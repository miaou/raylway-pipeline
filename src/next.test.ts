import { Result } from 'ts-railway'
import pipe from './index'

describe('next()', () => {
  it('Mix', async () => {
    const result = await pipe(Result.success(1))
      .next(value => value + 1)
      .next(value => Promise.resolve(value + 1))
      .next(value => Result.success(value + 1))
      .next(value => Promise.resolve(Result.success(value + 1)))
      .end()

    expect(result).toEqual({ tag: 'success', success: 5 })
  })
})
