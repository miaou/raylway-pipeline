import { Result } from 'ts-railway'
import pipe from './index'

describe('next()', () => {
  it('Full', async () => {
    const result = await pipe(Result.success(1))
      .next(value => Promise.resolve(Result.success(value + 1)))
      .next(value => Promise.resolve(Result.success(`${value}`)))
      .next(value => Promise.resolve(Result.success(`${value}`.length)))
      .end()

    expect(result).toEqual({ tag: 'success', success: 1 })
  })
})
