import { Result } from 'ts-railway'
import pipe from './index'

describe('Pipe', () => {
  it('When success', async () => {
    const result = await pipe(1)
      .next(value => value + 1)
      .next(value => Promise.resolve(value + 10))
      .next(value =>
        !value ? Result.failure(value + 5) : Result.success(value + 5)
      )
      .next(value => value + 10)
      .next(value =>
        Promise.resolve(
          !value ? Result.failure('ouille') : Result.success(value + 5)
        )
      )
      .next(value => value + 10)
      .nextIfError(value => `Error ${value}`)
      .nextIfError(value => `Error aaa ${value}`)
      .next(value => Promise.resolve(value + 10))
      .next(value => Promise.resolve(value + 10))
      .end()

    expect(result).toEqual({ tag: 'success', success: 62 })
  })

  it('When error', async () => {
    const result = await pipe(1)
      .next(value => value)
      .next(value => Promise.resolve(value))
      .next(value => value + 10)
      .nextIfError(value => `Error ${value}`)
      .next(value => value + 1)
      .nextIfError(value => Promise.resolve(`Error ${value}`))
      .next(value => (!value ? Result.failure('na') : Result.success(1000)))
      .next(value => value)
      .next(value => value + 10)
      .next(value => Promise.resolve(value + 10))
      .next(value =>
        Promise.resolve(
          value
            ? Result.failure(`Ouille ${value + 100}`)
            : Result.success(value + 5)
        )
      )
      .nextIfError(value => `Error ${value}`)
      .nextIfError(() => {
        // Do Nothing
      })
      .next(value => Promise.resolve(value + 10))
      .end()

    expect(result).toEqual({ tag: 'failure', failure: 'Error Ouille 1120' })
  })

  // it('When next() return void should have fellow next() return type before', async () => {
  //   const result = await pipe(1)
  //     .next(value => value + 10)
  //     .next(() => {})
  //     .end()
  //
  //   expect(result).toEqual({ tag: 'failure', failure: 'Error Ouille 1120' })
  // })
})
