import pipe from './index'

describe('Throw', () => {
  it('When next() throw then return Error failure', async () => {
    const result = await pipe(1)
      .next((value) => {
        if (value) throw new Error('ouille')
        return value
      })
      .end()

    expect(result).toEqual({ failure: new Error('ouille'), tag: 'failure' })
  })

  it('When next() throw and nextIfError() then override default Error', async () => {
    const result = await pipe(1)
      .next((value) => {
        if (value) throw new Error('ouille')
        return value
      })
      .nextIfError(({ message }) => message)
      .end()

    expect(result).toEqual({ failure: 'ouille', tag: 'failure' })
  })

  it('When next() throw and nextIfError() return void then next nextIfError() should have default Error', async () => {
    const result = await pipe(1)
      .next((value) => {
        if (value) throw new Error('ouille')
        return value
      })
      .nextIfError(() => {})
      .nextIfError(value => value)
      .end()

    expect(result).toEqual({ failure: new Error('ouille'), tag: 'failure' })
  })

  it('When next() throw and nextIfError() return void then next nextIfError() should have nextIfError() return type', async () => {
    const result = await pipe(1)
      .next((value) => {
        if (value) throw new Error('ouille')
        return value
      })
      .nextIfError(({ message }) => message)
      .nextIfError(() => {})
      .nextIfError(value => value)
      .end()

    expect(result).toEqual({ failure: 'ouille', tag: 'failure' })
  })
})
