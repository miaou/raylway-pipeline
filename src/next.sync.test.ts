import pipe from './index'

describe('next()', () => {
  it('Full', async () => {
    const result = await pipe(1)
      .next(value => value + 1)
      .next(value => `${value}`)
      .next(value => `${value}`.length)
      .end()

    expect(result).toEqual({ tag: 'success', success: 1 })
  })

  it('Before if empty', async () => {
    const result = await pipe(1)
      .next(value => value + 1)
      .next(() => {})
      .next(value => value + 1)
      .end()

    expect(result).toEqual({ tag: 'success', success: 3 })
  })
})
