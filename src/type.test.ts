import pipe from './index'

describe('Type', () => {
  it('When next() then follow next() should have return type from before', async () => {
    const result = await pipe([1, 2, 3])
      .next(ids => Promise.resolve(ids.map(i => ({ id: i }))))
      .next((values) => {
        if (values.length > 0) throw new Error('No values')
        return Promise.resolve(values.map(({ id }) => ({ id, value: 'value' })))
      })
      .nextIfError(({ message }) => {
        console.log('Do nothing, Error detail:', message)
        return Promise.resolve()
      })
      .next(([first]) => Promise.resolve(first))
      .end()

    expect(result).toEqual({ tag: 'failure', failure: new Error('No values') })
  })
})
