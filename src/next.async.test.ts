import pipe from './index'

describe('next(() => Promise))', () => {
  it('Full', async () => {
    const result = await pipe(1)
      .next(value => Promise.resolve(value + 1))
      .next(value => Promise.resolve(`${value}`))
      .next(value => Promise.resolve(`${value}`.length))
      .end()

    expect(result).toEqual({ tag: 'success', success: 1 })
  })

  it('Before if empty', async () => {
    const result = await pipe(1)
      .next(value => Promise.resolve(value + 1))
      .next(() => Promise.resolve())
      .next(value => Promise.resolve(value + 1))
      .end()

    expect(result).toEqual({ tag: 'success', success: 3 })
  })
})
