import { match, P } from 'ts-pattern'
import { AsyncResult, FailureOf, Result, SomeResult } from 'ts-railway'
import {
  AsyncResult as AsyncResultType,
  Result as ResultType
} from 'ts-railway/dist/dts/types'
import { SuccessOf } from 'ts-railway/src/types'

export type PipeSuccess<T> = T extends SomeResult ? SuccessOf<T> : T
export type PipeFailure<T> = T extends SomeResult ? FailureOf<T> : Error
export type PipeNextSuccess<T, S> = T extends void
    ? S
    : T extends SomeResult<any, any>
        ? SuccessOf<T>
        : T extends Promise<infer I>
            ? I extends void ? S : I
            : T extends void
                ? S
                : T
export type PipeNextFailure<T, F> = T extends SomeResult ? FailureOf<T> : F
export type PipeNextErrorFailure<T, F> = T extends void
    ? F
    : T extends SomeResult<any, any>
        ? FailureOf<T>
        : T extends Promise<infer I>
            ? I extends void ? F : I
            : T extends void
                ? F
                : T

type Pipe<T, S = PipeSuccess<T>, F = PipeFailure<T>> = {
    next: <N>(f: (nextValue: S) => N) => Pipe<T, PipeNextSuccess<N, S>, PipeNextFailure<N, F>>
    nextIfError: <NE>(f: (nextValue: F) => NE) => Pipe<NE, S, PipeNextErrorFailure<NE, F>>
    end: () => SomeResult<S, F>;
}

const pipe = <T, S = PipeSuccess<T>, F = PipeFailure<T>>(value: T) : Pipe<T, S, F> => {
  const result: SomeResult<S, F> = match<any>(value)
    .when(
      v => typeof (v as Promise<S>).then === 'function',
      result => result as AsyncResultType<S, F>
    )
    .with({ tag: P._ }, result => result as ResultType<S, F>)
    .otherwise(value => Result.success(value as S))
  return {
    next: <N>(f: (nextValue: S) => N) => {
      return pipe<N, PipeNextSuccess<N, S>, PipeNextFailure<N, F>>(
        // @ts-ignore
        AsyncResult.flatMap(async (a) => {
          try {
            const t = await Promise.resolve(f(a))
            if (!t) return Result.success(a)
            return match(t)
              .with(
                // @ts-ignore
                { tag: P.string },
                result => result
              )
              .otherwise(value => Result.success(value))
          } catch (error: any) {
            return Result.failure(error)
          }
        }, result)
      )
    },
    nextIfError: <NE>(f: (nextValue: F) => NE) => {
      return pipe<NE, S, PipeNextErrorFailure<NE, F>>(
        // @ts-ignore
        AsyncResult.flatMapError(async (a) => {
          const t = await Promise.resolve(f(a))
          if (!t) return Result.failure(a)
          return (
            match(t)
            // @ts-ignore
              .with({ tag: P.string }, result => result)
              .otherwise(value => Result.failure(value))
          )
        }, result)
      )
    },
    end: () => result
  }
}

export default pipe
