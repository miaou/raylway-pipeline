import { Result } from 'ts-railway'
import pipe from './index'

describe('next(() => Result)', () => {
  it('Full', async () => {
    const result = await pipe(Result.success(1))
      .next(value => Result.success(value + 1))
      .next(value => Result.success(`${value}`))
      .next(value => Result.success(`${value}`.length))
      .end()

    expect(result).toEqual({ tag: 'success', success: 1 })
  })
})
